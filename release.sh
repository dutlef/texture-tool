#!/usr/bin/env bash

cmake --preset release
cmake --build build/release


mkdir -p release
cp -r build/release/assets release/
cp -r build/release/shaders release/
cp -r build/release/TextureTool release/

cmake --preset default