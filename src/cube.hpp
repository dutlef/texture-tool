#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "./application.hpp"
#include "./shader.hpp"

#include <assimp/Importer.hpp>	// C++ importer interface
#include <assimp/postprocess.h> // Post processing flags
#include <assimp/scene.h>		// Output data structure

class Cube
{
  public:
	Shader *shader;
	unsigned int VBO, EBO, VAO;
	Application *app;

	std::vector<float> vertices2;
	std::vector<unsigned int> indices2;

	Cube(Application *app)
	{
		this->app = app;

		compile_shaders();

		Assimp::Importer importer;

		auto gltf_path = "./assets/bevelled_cube.gltf";

		const aiScene *scene = importer.ReadFile(gltf_path, aiProcess_CalcTangentSpace | aiProcess_Triangulate);

		if (nullptr == scene)
		{
			std::printf("couldn't load gltf: %s\n", importer.GetErrorString());
			return;
		}

		auto mesh = scene->mMeshes[0];

		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			vertices2.push_back(mesh->mVertices[i].x);
			vertices2.push_back(mesh->mVertices[i].y);
			vertices2.push_back(mesh->mVertices[i].z);

			vertices2.push_back(mesh->mTextureCoords[0][i].x);
			vertices2.push_back(mesh->mTextureCoords[0][i].y);

			vertices2.push_back(mesh->mNormals[i].x);
			vertices2.push_back(mesh->mNormals[i].y);
			vertices2.push_back(mesh->mNormals[i].z);

			vertices2.push_back(mesh->mTangents[i].x);
			vertices2.push_back(mesh->mTangents[i].y);
			vertices2.push_back(mesh->mTangents[i].z);

			vertices2.push_back(mesh->mBitangents[i].x);
			vertices2.push_back(mesh->mBitangents[i].y);
			vertices2.push_back(mesh->mBitangents[i].z);
		}

		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			for (unsigned int j = 0; j < face.mNumIndices; j++)
			{
				indices2.push_back(face.mIndices[j]);
			}
		}

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);

		// TODO: buffer stuff when using different geometries
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		// glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
		glBufferData(GL_ARRAY_BUFFER, vertices2.size() * sizeof(float), &vertices2[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices2.size() * sizeof(unsigned int), &indices2[0], GL_STATIC_DRAW);

		// === VERTEX ATTRIBUTES

		int stride = 14 * sizeof(float);
		// position
		int attribLocation = 0;
		int vertexAttribSize = 3;
		glVertexAttribPointer(attribLocation, vertexAttribSize, GL_FLOAT, GL_FALSE, stride, (void *)0);
		glEnableVertexAttribArray(0);

		// tex coords
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void *)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		// normals
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride, (void *)(5 * sizeof(float)));
		glEnableVertexAttribArray(2);

		// tangents
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, stride, (void *)(8 * sizeof(float)));
		glEnableVertexAttribArray(3);

		// bitangents
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, stride, (void *)(11 * sizeof(float)));
		glEnableVertexAttribArray(4);

		glBindVertexArray(0);
	}

	void compile_shaders()
	{
		shader = new Shader("./shaders/cube.vert.glsl", "./shaders/cube.frag.glsl");
		shader->use();

		shader->setInt("u_cropped_color", 0);
		shader->setInt("u_cropped_displacement", 1);
		shader->setInt("u_cropped_normal", 2);
		shader->setInt("u_cropped_rough", 3);
		shader->setInt("u_cropped_metal", 4);
		shader->setInt("u_cropped_ao", 5);
	}

	void render(float time)
	{

		if (app->recompile_3d_view)
		{
			compile_shaders();
			app->recompile_3d_view = false;
		}

		auto cam_position = glm::vec3(0.0f, 0.0f, app->camera->zoom);
		auto up = glm::vec3(0.0f, 1.0f, 0.0f);
		auto front = glm::vec3(0.0f, 0.0f, -1.0f);
		auto view = glm::lookAt(cam_position, glm::vec3(0.f, 0.f, 0.f), up);

		bool left = app->mouse_left_down && app->mouse_left_x > app->window->panel_width;
		bool right = app->mouse_right_down && app->mouse_right_x > app->window->panel_width;

		if (left || right)
		{
			double x_diff = app->previous_mouse_x - app->mouse_x;
			double y_diff = app->previous_mouse_y - app->mouse_y;

			if (app->mouse_x < app->window->panel_width)
			{
				app->change_mouse = true;
				app->next_mouse_x = app->window->width - 1;
			}

			if (app->mouse_x > app->window->width)
			{
				app->change_mouse = true;
				app->next_mouse_x = app->window->panel_width + 1;
			}

			if (app->mouse_y < 0.f)
			{
				app->change_mouse = true;
				app->next_mouse_y = app->window->height - 1;
			}

			if (app->mouse_y > app->window->height)
			{
				app->change_mouse = true;
				app->next_mouse_y = 1;
			}

			if (left)
			{
				this->rotation.y -= x_diff * 0.006f;
				this->rotation.x -= y_diff * 0.006f;

				float pi_halfed = glm::pi<float>() / 2.0f;

				if (this->rotation.x > pi_halfed)
				{
					this->rotation.x = pi_halfed;
				}
				else if (this->rotation.x < -pi_halfed)
				{
					this->rotation.x = -pi_halfed;
				}
			}

			if (right)
			{
				app->rotating_light = true;
				app->light_rotation -= x_diff * 0.4f;
				if (app->light_rotation < 0.f)
				{
					app->light_rotation += 360.f;
				}
				else if (app->light_rotation > 360.f)
				{
					app->light_rotation -= 360.f;
				}
			}
		}

		glm::mat4 projection;
		projection = glm::perspective(glm::radians(45.f),
									  app->window->get_view3d_width() / app->window->get_view3d_height(), 0.1f, 100.0f);

		for (int i = 0; i < std::size(app->texture_maps); i++)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, (app->use_lowres_preview ? app->texture_maps_lowres : app->texture_maps)[i]);
		}

		shader->use();

		shader->setFloat("u_tiling_scale", app->texture_settings->tiling_scale);
		shader->setFloat("u_displacement_scale", app->texture_settings->displacement_scale);
		shader->setFloat("u_normal_scale", app->texture_settings->normal_scale);
		shader->setFloat("u_light_rotation", app->light_rotation);

		shader->setInt("u_view_mode", app->view_mode);
		shader->setBool("u_shaded", app->show_shaded_view);

		shader->setFloat("u_crop_width", (float)app->texture_settings->get_crop_width());
		shader->setFloat("u_crop_height", (float)app->texture_settings->get_crop_height());

		// projection
		int projectionLoc = glGetUniformLocation(shader->ID, "projection");
		glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		// view
		int viewLoc = glGetUniformLocation(shader->ID, "view");
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		// model
		glm::mat4 model = glm::mat4(1.0f);

		model = glm::translate(model, this->position);

		model = glm::rotate(model, this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::rotate(model, this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::rotate(model, this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

		model = glm::scale(model, this->scale);

		int modelLoc = glGetUniformLocation(shader->ID, "model");
		glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

		// drawing
		// glDrawArrays(GL_TRIANGLES, 0, 36);

		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, indices2.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}

	glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 scale = glm::vec3(0.8f);
	glm::vec3 rotation = glm::vec3(0.0f, 0.0f, 0.0f);
};