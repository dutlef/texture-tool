#include "glad/glad.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <fmt/core.h>
// #include "./glad/glad.h"
#include "./cube.hpp"
#include "./shader.hpp"
#include "./ui_mesh.hpp"
#include "./ui_panel.hpp"
#include "./view2d_mesh.hpp"
#include "GLFW/glfw3.h"
#include <glm/glm.hpp>
#include <stb_image_write.h>
#include <tiffio.h>

#include <array>
#include <filesystem>
#include <stdio.h>
#include <string>
#include <thread>
#include <vector>

#include "application.hpp"

static void glfw_error_callback(int error, const char *description)
{
	fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

bool colorize;

static Settings *settings;

static Application *app;

static View2DMesh *view2d_mesh;

void calculate_blur_map()
{
	std::printf("caluclate blor in thread \n");
	app->blur_map_thread_status = BlurMapStatus::Calculating;

	auto blur_path = app->texture_settings->get_blur_map_path();
	auto lowres_path = std::string(blur_path).replace(blur_path.find("blur_map"), 8, "low_res");
	auto flip_path = std::string(blur_path).replace(blur_path.find("blur_map"), 8, "flip");
	auto flop_path = std::string(blur_path).replace(blur_path.find("blur_map"), 8, "flop");
	auto flipflop_path = std::string(blur_path).replace(blur_path.find("blur_map"), 8, "flipflop");
	auto combined_path = std::string(blur_path).replace(blur_path.find("blur_map"), 8, "combined");
	auto seamless_blur = std::string(blur_path).replace(blur_path.find("blur_map"), 8, "seamless_blur");

	// scaling down the diffuse texture
	std::string scale_command =
		fmt::format("convert -scale 12.5% \"{}\" \"{}\"", app->texture_settings->diffuse_import_path, lowres_path);

	if (int return_code = system(scale_command.c_str()) != 0)
	{
		std::printf("Failed to scale down image. Returned non-zero: %i", return_code);
		app->blur_map_thread_status = BlurMapStatus::Failed;
		return;
	};

	// creating mirrored versions
	std::string flip_flop_command =
		fmt::format("convert -flip \"{}\" \"{}\" && convert -flop \"{}\" \"{}\" && convert -flip -flop \"{}\" \"{}\"",
					lowres_path, flip_path, lowres_path, flop_path, lowres_path, flipflop_path);

	if (int return_code = system(flip_flop_command.c_str()) != 0)
	{
		std::printf("Failed to create flip-flop maps. Returned non-zero: %i", return_code);
		app->blur_map_thread_status = BlurMapStatus::Failed;
		return;
	};

	// combining the flip-flop maps and the original one
	std::string combine_command = "";

	if (app->texture_settings->tiling_mode == TextureSettings::TilingMode::BothAxes)
	{
		combine_command = fmt::format(
			"montage \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" -tile 3x3 -geometry +0+0 \"{}\"",
			flipflop_path, flip_path, flipflop_path, flop_path, lowres_path, flop_path, flipflop_path, flip_path,
			flipflop_path, combined_path);
	}
	else if (app->texture_settings->tiling_mode == TextureSettings::TilingMode::Vertical)
	{
		combine_command = fmt::format(
			"montage \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" -tile 3x3 -geometry +0+0 \"{}\"",
			flip_path, flip_path, flip_path, lowres_path, lowres_path, lowres_path, flip_path, flip_path, flip_path,
			combined_path);
	}
	else
	{
		combine_command = fmt::format(
			"montage \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" -tile 3x3 -geometry +0+0 \"{}\"",
			flop_path, lowres_path, flop_path, flop_path, lowres_path, flop_path, flop_path, lowres_path, flop_path,
			combined_path);
	}

	if (int return_code = system(combine_command.c_str()) != 0)
	{
		std::printf("Failed to combine the flip-flop maps. Returned non-zero: %i", return_code);
		app->blur_map_thread_status = BlurMapStatus::Failed;
		return;
	};

	// blur the map and crop it
	float texture_size = std::min(app->texture_settings->texture_width, app->texture_settings->texture_height);
	float sigma = texture_size / 125;

	int width = app->texture_settings->texture_width / 8;
	int height = app->texture_settings->texture_height / 8;

	std::string blur_command = fmt::format("convert -blur 0x{} -crop {}x{}+{}+{} \"{}\" \"{}\"", sigma, width, height,
										   width, height, combined_path, app->texture_settings->get_blur_map_path());

	if (int return_code = system(blur_command.c_str()) != 0)
	{
		std::printf("Failed to blur image. Returned non-zero: %i", return_code);
		app->blur_map_thread_status = BlurMapStatus::Failed;
		return;
	}

	app->blur_map_thread_status = BlurMapStatus::MapCreated;
}

void update_texture(unsigned int texture, int width, int height)
{
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void update_buffer(unsigned int buffer, int width, int height)
{
	glBindRenderbuffer(GL_RENDERBUFFER, buffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, buffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		std::printf("framebuffer error");
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void set_view_mode(int view_mode)
{
	if (app->view_mode == view_mode)
	{
		app->show_shaded_view = !app->show_shaded_view;
	}
	else
	{
		app->show_shaded_view = false;
	}

	app->view_mode = view_mode;
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS)
	{
		if (key == GLFW_KEY_X)
		{
			app->recompile_2d_view = true;
			app->recompile_3d_view = true;
			app->render_preview();
		}

		if (key == GLFW_KEY_Q)
		{
			set_view_mode(0);
		}

		if (key == GLFW_KEY_W)
		{
			set_view_mode(1);
		}

		if (key == GLFW_KEY_E)
		{
			set_view_mode(2);
		}

		if (key == GLFW_KEY_R)
		{
			set_view_mode(3);
		}

		if (key == GLFW_KEY_T)
		{
			set_view_mode(4);
		}

		if (key == GLFW_KEY_Y)
		{
			set_view_mode(5);
		}
	}
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	app->window->width = width;
	app->window->height = height;

	update_texture(app->texture_3dview, app->window->get_view3d_width(), app->window->get_view3d_height());
	update_buffer(app->rbo, app->window->get_view3d_width(), app->window->get_view3d_height());
	// update_view3d_buffer();

	update_texture(app->texture_2dview, app->window->get_view2d_width(), app->window->get_view2d_height());
	update_buffer(app->renderbuffer_2dview, app->window->get_view2d_width(), app->window->get_view2d_height());
	// update_view2d_buffer();
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
	if (app->mouse_x < app->window->panel_width)
	{
		return;
	}

	app->camera->zoom -= yoffset * 0.01f * app->camera->zoom * 5.0f;

	if (app->camera->zoom < 0.3f)
	{
		app->camera->zoom = 0.3f;
	}

	if (app->camera->zoom > 100.0f)
	{
		app->camera->zoom = 100.0f;
	}
}

void mouse_button_callback(GLFWwindow *window, int button, int action, int mods)
{
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	if (button == GLFW_MOUSE_BUTTON_RIGHT)
	{
		app->mouse_right_down = (action == GLFW_PRESS);
		app->mouse_right_x = xpos;
		app->mouse_right_y = ypos;
	}

	if (button == GLFW_MOUSE_BUTTON_LEFT)
	{
		app->mouse_left_down = (action == GLFW_PRESS);
		app->mouse_left_x = xpos;
		app->mouse_left_y = ypos;

		if (action == GLFW_PRESS)
		{
			app->texture_settings->crop_before_mousedown = app->texture_settings->crop;
		}

		if (action == GLFW_RELEASE)
		{
			if (app->texture_settings->crop_before_mousedown != app->texture_settings->crop)
			{
				app->render_preview();
			}
		}
	}
}

static void cursor_position_callback(GLFWwindow *window, double xpos, double ypos)
{
	// app->previous_mouse_x = app->mouse_x;
	// app->previous_mouse_y = app->mouse_y;

	// app->mouse_x = xpos;>
	// app->mouse_y = ypos;

	// std::printf("x %f // y %f\n", app->mouse_x, app->mouse_y);
}

void handle_dropped_file(const char *path)
{
	std::printf("dropped %s\n", path);

	std::filesystem::path file_path{path};
	std::filesystem::path file_name = file_path.filename();
	std::string export_path{path};
	export_path.replace(export_path.find(".png"), 4, "_seamless.tiff");

	std::string diffuse_identifiers[] = {
		"diffuse",		"color", "basecolor", "albedo",
		"basetexbaked", // xNormal
	};

	std::string displacement_identifiers[] = {
		"disp",
		"displacement",
		"height",
		"heights",
	};

	std::string normal_identifiers[] = {
		"normal", "norm", "normals", "normaldirectx", "normalopengl",
	};

	std::string ao_identifiers[] = {
		"AO",
		"ambientocclusion",
	};

	std::string rough_identifiers[] = {
		"rough",
		"roughness",
	};

	std::string metal_identifiers[] = {
		"metallic",
		"metal",
	};

	std::vector<std::string> tokens;
	std::string delimiters = "_ -.";
	int start = 0;
	int end = 0;

	while (end != std::string::npos)
	{
		end = file_name.string().find_first_of(delimiters, start);
		std::string token = file_name.string().substr(start, end - start);
		std::transform(token.begin(), token.end(), token.begin(), ::tolower); // wtf is this standard library
		tokens.push_back(token);
		start = end + 1;
	}

	for (std::string token : tokens)
	{
		for (std::string str : diffuse_identifiers)
		{
			if (str == token)
			{
				view2d_mesh->set_texture(file_path, TextureMapType::Diffuse);
				app->texture_settings->diffuse_import_path = file_path;
				app->texture_settings->diffuse_export_path = export_path;
			}
		}

		for (std::string str : displacement_identifiers)
		{
			if (str == token)
			{
				view2d_mesh->set_texture(file_path, TextureMapType::Displacement);
				app->texture_settings->displacement_export_path = export_path;
			}
		}

		for (std::string str : normal_identifiers)
		{
			if (str == token)
			{
				view2d_mesh->set_texture(file_path, TextureMapType::Normal);
				app->texture_settings->normal_export_path = export_path;
			}
		}

		for (std::string str : rough_identifiers)
		{
			if (str == token)
			{
				view2d_mesh->set_texture(file_path, TextureMapType::Roughness);
				app->texture_settings->rough_export_path = export_path;
			}
		}

		for (std::string str : ao_identifiers)
		{
			if (str == token)
			{
				view2d_mesh->set_texture(file_path, TextureMapType::AmbientOcclusion);
				app->texture_settings->ao_export_path = export_path;
			}
		}

		for (std::string str : metal_identifiers)
		{
			if (str == token)
			{
				view2d_mesh->set_texture(file_path, TextureMapType::Metallic);
				app->texture_settings->metal_export_path = export_path;
			}
		}
	}

	app->render_preview();
}

void drop_callback(GLFWwindow *window, int count, const char **paths)
{
	int i;
	for (i = 0; i < count; i++)
	{
		handle_dropped_file(paths[i]);
	}
}

void start_app(GLFWwindow *window, ImGuiIO &io)
{
	glEnable(GL_DEPTH_TEST);
	auto ui_mesh = new UIMesh(app);

	// UIColors *ui_colors = new UIColors();
	// AppConfig *config = new AppConfig;
	// config->ui_colors = ui_colors;

	// ui_colors->background = hex_to_imvec4("#151617");
	// ui_colors->background_darker = hex_to_imvec4("#0F1011");
	// ui_colors->background_darkest = hex_to_imvec4("#000000");
	// ui_colors->border = hex_to_imvec4("#000000");
	// ui_colors->text = hex_to_imvec4("#ffffff");

	UIPanel *ui_panel = new UIPanel(app);

	// 3d view
	unsigned int framebuffer_3dview;
	glGenFramebuffers(1, &framebuffer_3dview);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_3dview);

	glGenTextures(1, &app->texture_3dview);

	update_texture(app->texture_3dview, app->window->get_view3d_width(), app->window->get_view3d_height());
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, app->texture_3dview, 0);

	glGenRenderbuffers(1, &app->rbo);
	update_buffer(app->rbo, app->window->get_view3d_width(), app->window->get_view3d_height());

	// 2d view
	unsigned int framebuffer_2dview;

	glGenFramebuffers(1, &framebuffer_2dview);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_2dview);

	glGenTextures(1, &app->texture_2dview);
	update_texture(app->texture_2dview, app->window->get_view2d_width(), app->window->get_view2d_height());
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, app->texture_2dview, 0);

	glGenRenderbuffers(1, &app->renderbuffer_2dview);
	update_buffer(app->renderbuffer_2dview, app->window->get_view2d_width(), app->window->get_view2d_height());

	// cropped texture maps
	unsigned int texture_framebuffers[6];
	unsigned int lowres_framebuffers[6];
	unsigned int export_framebuffers[6];

	int crop_preview_size = 4096;
	int crop_lowres_size = 512;

	for (int i = 0; i < std::size(texture_framebuffers); i++)
	{
		// preview
		glGenFramebuffers(1, &texture_framebuffers[i]);
		glBindFramebuffer(GL_FRAMEBUFFER, texture_framebuffers[i]);

		glGenTextures(1, &app->texture_maps[i]);
		update_texture(app->texture_maps[i], crop_preview_size, crop_preview_size);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, app->texture_maps[i], 0);

		glGenRenderbuffers(1, &app->renderbuffer_texture_maps);
		update_buffer(app->renderbuffer_texture_maps, crop_preview_size, crop_preview_size);

		// preview
		glGenFramebuffers(1, &lowres_framebuffers[i]);
		glBindFramebuffer(GL_FRAMEBUFFER, lowres_framebuffers[i]);

		glGenTextures(1, &app->texture_maps_lowres[i]);
		update_texture(app->texture_maps_lowres[i], crop_lowres_size, crop_lowres_size);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, app->texture_maps_lowres[i], 0);

		glGenRenderbuffers(1, &app->renderbuffer_texture_maps_lowres);
		update_buffer(app->renderbuffer_texture_maps_lowres, crop_lowres_size, crop_lowres_size);

		// buffer for export
		glGenFramebuffers(1, &export_framebuffers[i]);
		glBindFramebuffer(GL_FRAMEBUFFER, export_framebuffers[i]);

		glGenRenderbuffers(1, &app->export_render_buffer_objects[i]);
		glBindRenderbuffer(GL_RENDERBUFFER, app->export_render_buffer_objects[i]);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB16, 2048, 2048);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER,
								  app->export_render_buffer_objects[i]);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			std::printf("export framebuffer error: %i\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	// ---------

	auto cube = new Cube(app);

	view2d_mesh = new View2DMesh(app);

	// ==== 3d view

	app->render_preview();

	float previous_time = 0.f;

	while (!glfwWindowShouldClose(window))
	{
		float time = glfwGetTime();
		app->delta_time = time - previous_time;
		previous_time = time;

		app->idle_since += app->delta_time;

		if (app->idle_since > 0.3f && !app->already_rendered_preview)
		{
			app->render_preview();
		}

		app->previous_mouse_x = app->mouse_x;
		app->previous_mouse_y = app->mouse_y;
		glfwGetCursorPos(window, &app->mouse_x, &app->mouse_y);
		app->next_mouse_x = app->mouse_x;
		app->next_mouse_y = app->mouse_y;

		app->hovered_window = Windows::SettingsPanel;

		if (app->mouse_x > app->window->panel_width)
		{
			app->hovered_window = Windows::View3D;
		}
		else if (app->mouse_y > app->window->panel_height)
		{
			app->hovered_window = Windows::View2D;
		}

		if (app->calculate_blur_map)
		{
			std::printf("calculating blur \n");

			// TODO: learn how to this stuff properly, but idc at the moment
			new std::thread(calculate_blur_map);
			app->calculate_blur_map = false;
		}

		if (app->blur_map_thread_status == BlurMapStatus::MapCreated)
		{
			view2d_mesh->set_texture(app->texture_settings->get_blur_map_path(), TextureMapType::BlurMap);
			app->blur_map_thread_status = BlurMapStatus::MapLoaded;
			app->render_preview();
		}

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// 2d view
		glViewport(0, 0, app->window->get_view2d_width(), app->window->get_view2d_height());
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_2dview);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float window_ratio = app->window->get_view2d_width() / app->window->get_view2d_height();

		glEnable(GL_DEPTH_TEST);
		view2d_mesh->render(false, app->view_mode);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// cropped text maps
		if (app->rerender_preview)
		{
			int width = app->use_lowres_preview ? crop_lowres_size : crop_preview_size;
			int height = app->use_lowres_preview ? crop_lowres_size : crop_preview_size;
			auto fb = app->use_lowres_preview ? lowres_framebuffers : texture_framebuffers;

			glViewport(0, 0, width, height);

			for (int i = 0; i < std::size(texture_framebuffers); i++)
			{
				glBindFramebuffer(GL_FRAMEBUFFER,
								  (app->use_lowres_preview ? lowres_framebuffers : texture_framebuffers)[i]);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				view2d_mesh->render(true, i);
			}

			// std::printf("rendering %ix%i\n", width, height);
		}

		if (app->export_textures)
		{
			int width = app->texture_settings->get_crop_width();
			int height = app->texture_settings->get_crop_height();

			if (app->use_custom_export_size)
			{
				width = app->export_size.x;
				height = app->export_size.y;
			}

			width = (width % 2) == 0 ? width : width + 1;
			height = (height % 2) == 0 ? height : height + 1;

			glViewport(0, 0, width, height);

			app->export_textures = false;

			std::string filenames[6] = {
				app->texture_settings->diffuse_export_path, app->texture_settings->displacement_export_path,
				app->texture_settings->normal_export_path,	app->texture_settings->rough_export_path,
				app->texture_settings->metal_export_path,	app->texture_settings->ao_export_path,

			};

			for (int i = 0; i < std::size(texture_framebuffers); i++)
			{

				glBindFramebuffer(GL_FRAMEBUFFER, export_framebuffers[i]);

				glBindRenderbuffer(GL_RENDERBUFFER, app->export_render_buffer_objects[i]);
				glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB16, width, height);

				// glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER,
				// app->export_render_buffer_objects[i]);

				if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
				{
					std::printf("export framebuffer error: %i\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
				}
				// ====

				glClearColor(1.0, 0.5, 1.0, 1.0);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				view2d_mesh->render(true, i, true);

				int channels = i == 1 ? 1 : 3; // grayscale map for displacement, RGB otherwise

				glPixelStorei(GL_PACK_ALIGNMENT, 4);
				glReadBuffer(GL_FRONT);

				std::printf("exporting %s\n", filenames[i].c_str());

				std::vector<uint16_t> pixels(width * height * channels); // RGBA
				glReadPixels(0, 0, width, height, channels == 1 ? GL_RED : GL_RGB, GL_UNSIGNED_SHORT, pixels.data());

				TIFF *tiff = TIFFOpen(filenames[i].c_str(), "w");
				if (!tiff)
				{
					std::printf("Could not open TIFF file for writing: %s\n", filenames[i].c_str());
					continue;
				}

				TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, width);
				TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, height);

				TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 16);
				TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
				TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, channels == 1 ? PHOTOMETRIC_MINISBLACK : PHOTOMETRIC_RGB);

				TIFFSetField(tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);

				// https://www.awaresystems.be/imaging/tiff/tifftags/samplesperpixel.html
				TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, channels == 1 ? 1 : 3);

				for (int row = 0; row < height; row++)
				{
					if (TIFFWriteScanline(tiff, pixels.data() + (row * width * channels), row) < 0)
					{
						std::printf("Could not write TIFF scanline.");
						break;
					}
				}

				TIFFClose(tiff);
			}
		}

		// 3d view
		glViewport(0, 0, app->window->get_view3d_width(), app->window->get_view3d_height());
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_3dview);
		glClearColor(app->theme->background_darker.x, app->theme->background_darker.y, app->theme->background_darker.z,
					 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		cube->render(time);

		// plane2D->render(settings, window_ratio, colorize, settings->transition_sharpness,
		// settings->transition_width);

		// cube->render(time);

		// rest
		app->rerender_preview = false;

		glViewport(0, 0, app->window->width, app->window->height);
		glBindFramebuffer(GL_FRAMEBUFFER, 0); // back to default

		glClear(GL_COLOR_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		ui_mesh->render();

		ui_panel->render(io, window);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(window);

		if (app->texture_settings->crop_previous_frame != app->texture_settings->crop)
		{
			app->render_preview_lowres();
		}

		app->texture_settings->crop_previous_frame = app->texture_settings->crop;

		if (app->change_mouse)
		{
			app->mouse_x = app->next_mouse_x;
			app->mouse_y = app->next_mouse_y;
			glfwSetCursorPos(window, app->mouse_x, app->mouse_y);
			app->change_mouse = false;
		}

		// TODO: change cursor when rotating light
		// if (app->rotating_light)
		// {
		//     glfwSetCursor(window, app->resize_vertical_cursor);
		//     std::printf("cursor horizontal \n");
		// }
		// else
		// {
		//     glfwSetCursor(window, app->default_cursor);
		// }
	}

	glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w,
				 clear_color.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDeleteFramebuffers(1, &framebuffer_3dview);
}

// Main code
int main(int, char **)
{
	settings = new Settings();
	app = new Application();

	glfwSetErrorCallback(glfw_error_callback);
	if (!glfwInit())
		return 1;

	const char *glsl_version = "#version 330 core";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Create window with graphics context
	GLFWwindow *window = glfwCreateWindow(1280, 720, "Texture Tool", nullptr, nullptr);
	if (window == nullptr)
		return 1;
	glfwMakeContextCurrent(window);
	gladLoadGL();

	glfwSwapInterval(1); // Enable vsync

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	int display_w, display_h;
	glfwGetFramebufferSize(window, &display_w, &display_h);
	app->window->width = display_w;
	app->window->height = display_h;

	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetDropCallback(window, drop_callback);
	glfwSetKeyCallback(window, key_callback);

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();
	(void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;  // Enable Gamepad Controls

	ImGui::StyleColorsDark();

	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(window, true);

	app->default_font = io.Fonts->AddFontFromFileTTF("./assets/RadioCanadaBig-Regular.ttf", 15.0f);
	app->bold_font = io.Fonts->AddFontFromFileTTF("./assets/RadioCanadaBig-Bold.ttf", 15.0f);

	ImGui_ImplOpenGL3_Init(glsl_version);

	GLint maxTextureSize;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
	std::cout << "Max texture size: " << maxTextureSize << std::endl;

	start_app(window, io);

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}
