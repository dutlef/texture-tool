#pragma once

#include "util.hpp"
#include <atomic>
#include <filesystem>
#include <thread>
#include <vector>

#include "GLFW/glfw3.h"
#include "imgui.h"

struct Vec2
{
	float x;
	float y;

	Vec2(float x, float y) : x(x), y(y)
	{
	}

	Vec2(float xy) : x(xy), y(xy)
	{
	}
};

struct TextureCrop
{
	float horizontal_start = 0.0f;
	float horizontal_end = 1.0f;

	float vertical_start = 0.0f;
	float vertical_end = 1.f;

	float transition_sharpness = 7.0f;
	float transition_width = 0.1f;
	float transition_offset;

	float value_equalization = 0.5f;
};

struct PatchSettings
{
	Vec2 coords = Vec2(0.6f, 0.8f);
	Vec2 source = Vec2(0.1f, 0.1f);
	Vec2 size = Vec2(0.01f, 0.01f);

	float sharpness = 1.0f;
	float displacement_offset = 0.0f;
	float displacement_scale = 1.0f;
	float color_value = 1.0f;
	float color_pow = 1.0f;
	float color_saturation = 1.0f;
};

bool operator==(const TextureCrop &a, const TextureCrop &b)
{
	return (a.horizontal_start == b.horizontal_start && a.horizontal_end == b.horizontal_end &&
			a.vertical_start == b.vertical_start && a.vertical_end == b.vertical_end &&
			a.transition_sharpness == b.transition_sharpness && a.transition_offset == b.transition_offset &&
			a.value_equalization == b.value_equalization && a.transition_width == b.transition_width);
}

bool operator!=(const TextureCrop &a, const TextureCrop &b)
{
	return !(a == b);
}

struct TextureSettings
{
	int texture_width;
	int texture_height;

	TextureCrop crop;
	TextureCrop crop_before_mousedown;
	TextureCrop crop_previous_frame;

	int current_patch_index = 0;

	std::vector<PatchSettings> patches = {};

	std::vector<glm::vec2> patch_coords = {};
	glm::vec2 patch_coords_arr[10];
	std::vector<glm::vec2> patch_sources = {};
	glm::vec2 patch_sources_arr[10];
	std::vector<glm::vec2> patch_sizes = {};
	glm::vec2 patch_sizes_arr[10];

	float patch_sharpnesses_arr[10];
	float patch_displacement_offset_arr[10];
	float patch_displacement_scale_arr[10];
	float patch_color_value_arr[10];
	float patch_color_pow_arr[10];
	float patch_color_saturation_arr[10];

	int add_patch()
	{
		patches.push_back(PatchSettings());

		patch_coords.push_back(glm::vec2(0.75, 0.75));
		patch_sources.push_back(glm::vec2(0.25, 0.25));
		patch_sizes.push_back(glm::vec2(0.01, 0.01));

		current_patch_index = patch_coords.size() - 1;

		return current_patch_index;
	}

	void update_patch_arrays()
	{
		for (int i = 0; i < 10; i++)
		{
			if (i < patch_coords.size())
			{
				PatchSettings patch = patches[i];

				patch_coords_arr[i] = patch_coords[i];
				patch_sources_arr[i] = patch_sources[i];
				patch_sizes_arr[i] = patch_sizes[i];

				patch_sharpnesses_arr[i] = patch.sharpness;

				patch_displacement_offset_arr[i] = patch.displacement_offset;
				patch_displacement_scale_arr[i] = patch.displacement_scale;

				patch_color_value_arr[i] = patch.color_value;
				patch_color_pow_arr[i] = patch.color_pow;
				patch_color_saturation_arr[i] = patch.color_saturation;
			}
			else
			{
				patch_coords_arr[i] = glm::vec2(0.f);
				patch_sources_arr[i] = glm::vec2(0.f);
				patch_sharpnesses_arr[i] = 0.f;
			}
		}
	}

	bool fit_to_crop = false;
	bool show_color = true;

	bool show_demo_window = false;

	float tiling_scale = 2.0f;
	float displacement_scale = 0.1f;
	float normal_scale = 1.0f;

	enum TilingMode
	{
		BothAxes = 0,
		Horizontal = 1,
		Vertical = 2,
	};

	TilingMode tiling_mode;

	std::string diffuse_import_path = std::string("");
	std::filesystem::path diffuse_export_path = std::string("");
	std::filesystem::path displacement_export_path = std::string("");
	std::filesystem::path normal_export_path = std::string("");
	std::filesystem::path rough_export_path = std::string("");
	std::filesystem::path ao_export_path = std::string("");
	std::filesystem::path metal_export_path = std::string("");

	std::string get_blur_map_path()
	{
		return std::string(diffuse_import_path).replace(diffuse_import_path.find(".png"), 4, "_blur_map.png");
	}

	int get_crop_width()
	{
		return texture_width * (crop.horizontal_end - crop.horizontal_start);
	}

	int get_crop_height()
	{
		return texture_height * (crop.vertical_end - crop.vertical_start);
	}

	int get_horizontal_pixel_offset()
	{
		return crop.horizontal_start * texture_width;
	}

	int get_vertical_pixel_offset()
	{
		return crop.vertical_start * texture_height;
	}
};

struct Window
{
	float width;
	float height;
	float panel_width = 500.f;
	float panel_height = 400.f;

	float get_view3d_width()
	{
		return width - panel_width;
	}

	float get_view3d_height()
	{
		return height;
	}

	float get_view2d_width()
	{
		return panel_width;
	}

	float get_view2d_height()
	{
		return height - panel_height;
	}
};

struct Theme
{
	ImVec4 background;
	ImVec4 background_darker;
	ImVec4 background_darkest;

	ImVec4 border;

	ImVec4 text;
};

struct ViewCamera
{
	float zoom = 3.f;
};

enum Windows
{
	SettingsPanel,
	View2D,
	View3D,
};

enum BlurMapStatus
{
	None,
	Calculating,
	MapCreated,
	MapLoaded,
	Failed,
};

class Application
{
  public:
	TextureSettings *texture_settings;

	bool use_custom_export_size = false;
	glm::vec2 export_size = glm::vec2(0);

	Window *window;
	Theme *theme;
	ViewCamera *camera;

	ImFont *default_font;
	ImFont *bold_font;

	bool rerender_preview = false;
	bool use_lowres_preview = false;

	bool calculate_blur_map = false;
	std::atomic<BlurMapStatus> blur_map_thread_status;

	unsigned int texture_3dview;
	unsigned int rbo;

	unsigned int texture_2dview;
	unsigned int renderbuffer_2dview;

	unsigned int texture_maps[6];
	unsigned int texture_maps_lowres[6];
	// unsigned int texture_normal;
	// unsigned int texture_color;
	unsigned int renderbuffer_texture_maps;
	unsigned int renderbuffer_texture_maps_lowres;
	unsigned int export_render_buffer_objects[6];

	int view_mode = 1;
	bool show_shaded_view = true;

	bool mouse_right_down = false;
	bool mouse_left_down = false;

	int mouse_right_x;
	int mouse_right_y;

	int mouse_left_x;
	int mouse_left_y;

	double mouse_x;
	double mouse_y;

	double previous_mouse_x;
	double previous_mouse_y;

	bool change_mouse = false;
	double next_mouse_x;
	double next_mouse_y;

	float light_rotation = 180.f;

	Windows hovered_window;

	GLFWcursor *default_cursor;
	GLFWcursor *resize_horizontal_cursor;
	GLFWcursor *resize_vertical_cursor;

	bool rotating_light = false;

	bool recompile_2d_view = false;
	bool recompile_3d_view = false;

	bool export_textures = false;

	float idle_since;
	bool already_rendered_preview = false;

	float delta_time;

	float get_fps()
	{
		return 1.0 / delta_time;
	}

	Application()
	{
		texture_settings = new TextureSettings();
		window = new Window();
		camera = new ViewCamera();

		default_cursor = glfwCreateStandardCursor(GLFW_CURSOR_NORMAL);
		resize_horizontal_cursor = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
		resize_vertical_cursor = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);

		theme = new Theme();
		theme->background = hex_to_imvec4("#151617");
		theme->background_darker = hex_to_imvec4("#0F1011");
		theme->background_darkest = hex_to_imvec4("#000000");
		theme->border = hex_to_imvec4("#000000");
		theme->text = hex_to_imvec4("#ffffff");
	}

	~Application()
	{
		glfwDestroyCursor(default_cursor);
		glfwDestroyCursor(resize_horizontal_cursor);
		glfwDestroyCursor(resize_vertical_cursor);
	}

	void render_preview_lowres()
	{
		rerender_preview = true;
		use_lowres_preview = true;
		already_rendered_preview = false;
		idle_since = 0.f;
	}

	void render_preview()
	{
		rerender_preview = true;
		already_rendered_preview = true;
		use_lowres_preview = false;
	}
};
