#pragma once

#include "./application.hpp"
#include "./shader.hpp"
#include "./util.hpp"
#include "GLFW/glfw3.h"
#include "glad/glad.h"
#include <glm/glm.hpp>

#include <utility>

class View2DMesh
{

  public:
	unsigned int VBO, EBO, VAO;
	Shader *shader;
	Application *app;
	unsigned int texture_disp;
	unsigned int texture_col;
	unsigned int texture_nrm;
	unsigned int texture_metal;
	unsigned int texture_rough;
	unsigned int texture_ao;
	unsigned int texture_blur;

	View2DMesh(Application *app)
	{
		this->app = app;

		texture_disp = load_texture("./assets/empty_displacement.png", &app->texture_settings->texture_width,
									&app->texture_settings->texture_height);
		texture_col = load_texture("./assets/empty_diffuse.png", &app->texture_settings->texture_width,
								   &app->texture_settings->texture_height);
		texture_nrm = load_texture("./assets/empty_normal.png", &app->texture_settings->texture_width,
								   &app->texture_settings->texture_height);
		texture_rough = load_texture("./assets/empty_roughness.png");
		texture_metal = load_texture("./assets/empty_metallic.png");
		texture_ao = load_texture("./assets/empty_ao.png");

		compile_shaders();

		float vertices[] = {// positions      // tex coords
							1.f,  1.f,	0.f, 1.f, 1.f, 1.f,	 -1.f, 0.f, 1.f, 0.f,
							-1.f, -1.f, 0.f, 0.f, 0.f, -1.f, 1.f,  0.f, 0.f, 1.f};

		unsigned int indices[] = {0, 1, 3, 1, 2, 3};

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		// glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	}

	void set_texture(std::string path, TextureMapType type)
	{
		if (type == TextureMapType::Diffuse)
		{
			texture_col = load_texture(path.c_str(), &app->texture_settings->texture_width,
									   &app->texture_settings->texture_height);
		}
		else if (type == TextureMapType::Displacement)
		{
			texture_disp = load_texture(path.c_str(), &app->texture_settings->texture_width,
										&app->texture_settings->texture_height);
		}
		else if (type == TextureMapType::Normal)
		{
			texture_nrm = load_texture(path.c_str(), &app->texture_settings->texture_width,
									   &app->texture_settings->texture_height);
		}
		else if (type == TextureMapType::Roughness)
		{
			texture_rough = load_texture(path.c_str());
		}
		else if (type == TextureMapType::AmbientOcclusion)
		{
			texture_ao = load_texture(path.c_str());
		}
		else if (type == TextureMapType::Metallic)
		{
			texture_metal = load_texture(path.c_str());
		}
		else if (type == TextureMapType::BlurMap)
		{
			texture_blur = load_texture(path.c_str());
		}
	}

	void compile_shaders()
	{
		shader = new Shader("./shaders/view2d_mesh.vert.glsl", "./shaders/view2d_mesh.frag.glsl");
		shader->use();
		shader->setInt("u_texture_disp", 0);
		shader->setInt("u_texture_col", 1);
		shader->setInt("u_texture_nrm", 2);
		shader->setInt("u_texture_rough", 3);
		shader->setInt("u_texture_metal", 4);
		shader->setInt("u_texture_ao", 5);
		shader->setInt("u_texture_blur", 6);
	}

	void render(bool crop = false, int map_index = 0, bool flip_y = false)
	{
		shader->use();

		if (app->recompile_2d_view)
		{
			compile_shaders();
			app->recompile_2d_view = false;
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_disp);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture_col);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, texture_nrm);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, texture_rough);

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, texture_metal);

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, texture_ao);

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, texture_blur);

		shader->setFloat("u_view2d_width", app->window->get_view2d_width());
		shader->setFloat("u_view2d_height", app->window->get_view2d_height());

		shader->setFloat("u_vertical_start", app->texture_settings->crop.vertical_start);
		shader->setFloat("u_vertical_end", app->texture_settings->crop.vertical_end);
		shader->setFloat("u_horizontal_start", app->texture_settings->crop.horizontal_start);
		shader->setFloat("u_horizontal_end", app->texture_settings->crop.horizontal_end);

		shader->setVec3("u_background_darker", app->theme->background_darker.x, app->theme->background_darker.y,
						app->theme->background_darker.z);

		shader->setBool("u_crop", crop);

		// blender seems to ignore the tiff orientation tag, so i'm flipping the uv coords vertically when
		// exporting it to disk
		shader->setBool("u_flip_y", flip_y);

		shader->setInt("u_map_index", map_index);

		shader->setInt("u_tiling_mode", static_cast<int>(app->texture_settings->tiling_mode));

		shader->setFloat("u_transition_sharpness", app->texture_settings->crop.transition_sharpness);
		shader->setFloat("u_transition_width", app->texture_settings->crop.transition_width);
		shader->setFloat("u_transition_offset", app->texture_settings->crop.transition_offset);
		shader->setBool("u_colorize", app->texture_settings->show_color);

		app->texture_settings->update_patch_arrays();

		shader->setInt("u_patch_count", app->texture_settings->patch_coords.size());
		shader->setInt("u_active_patch", app->texture_settings->current_patch_index);
		shader->setVec2Array("u_patch_coords", app->texture_settings->patch_coords_arr[0], 10);
		shader->setVec2Array("u_patch_sources", app->texture_settings->patch_sources_arr[0], 10);
		shader->setVec2Array("u_patch_sizes", app->texture_settings->patch_sizes_arr[0], 10);

		shader->setFloatArray("u_patch_sharpnesses", app->texture_settings->patch_sharpnesses_arr, 10);
		shader->setFloatArray("u_patch_disp_offset", app->texture_settings->patch_displacement_offset_arr, 10);
		shader->setFloatArray("u_patch_disp_scale", app->texture_settings->patch_displacement_scale_arr, 10);
		shader->setFloatArray("u_patch_color_value", app->texture_settings->patch_color_value_arr, 10);
		shader->setFloatArray("u_patch_color_saturation", app->texture_settings->patch_color_saturation_arr, 10);
		shader->setFloatArray("u_patch_color_pow", app->texture_settings->patch_color_pow_arr, 10);

		if (app->blur_map_thread_status == BlurMapStatus::MapLoaded)
		{
			shader->setFloat("u_value_equalization", app->texture_settings->crop.value_equalization);
		}
		else
		{
			shader->setFloat("u_value_equalization", 0);
		}

		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
};
