#pragma once

#include "glad/glad.h"
#include "imgui.h"
#include <iostream>
#include <stb_image.h>
#include <stdio.h>

struct Settings
{
	float texture_width;
	float texture_height;

	float horizontal_start = 0.0f;
	float horizontal_end = 1.0f;

	float vertical_start = 0.0f;
	float vertical_end = 1.0f;

	bool fit_to_crop;
	bool show_color;

	bool show_demo_window = false;

	float transition_sharpness;
	float transition_width;

	int get_crop_width()
	{
		return texture_width * (horizontal_end - horizontal_start);
	}

	int get_crop_height()
	{
		return texture_height * (vertical_end - vertical_start);
	}

	int get_horizontal_pixel_offset()
	{
		return horizontal_start * texture_width;
	}

	int get_vertical_pixel_offset()
	{
		return vertical_start * texture_height;
	}
};

unsigned int load_texture(const char *path, int *width, int *height)
{
	std::printf("Loading image\n %s\n", path);
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load(path, width, height, &nrChannels, 0);
	std::printf("  %ix%i pixels\n", *width, *height);
	std::printf("  %i channels\n", nrChannels);

	if (data)
	{
		std::printf("  Successfully loaded.\n");

		auto format = GL_RGB;

		if (nrChannels == 4)
		{
			format = GL_RGBA;
		}
		else if (nrChannels == 2)
		{
			format = GL_RG;
		}
		else if (nrChannels == 1)
		{
			format = GL_RED;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, format, *width, *height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::printf("  Failed to load.\n");
	}

	stbi_image_free(data);

	return texture;
}

unsigned int load_texture(const char *path)
{
	int width, height;
	return load_texture(path, &width, &height);
}

unsigned int setup_texture(const char *path, Settings *settings)
{
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load(path, &width, &height, &nrChannels, 0);
	settings->texture_width = width;
	settings->texture_height = height;
	std::cout << "img: " << width << "/" << height << "/" << nrChannels << std::endl;

	if (data)
	{
		std::cout << "data fine" << std::endl;
		auto format = GL_RGB;

		if (nrChannels == 4)
		{
			format = GL_RGBA;
		}
		else if (nrChannels == 1)
		{
			format = GL_RED;
		}

		// TODO: second param, hää?
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}

	std::cout << "before free" << std::endl;
	stbi_image_free(data);

	return texture;
}

ImVec4 hex_to_imvec4(std::string hex)
{
	if (hex.length() != 7)
	{
		throw std::invalid_argument("hex has to be 7 characters long");
	}

	auto r = std::stoul(hex.substr(1, 2), nullptr, 16) / 255.f;
	auto g = std::stoul(hex.substr(3, 2), nullptr, 16) / 255.f;
	auto b = std::stoul(hex.substr(5, 2), nullptr, 16) / 255.f;

	return ImVec4(r, g, b, 1.f);
}

enum TextureMapType
{
	Diffuse,
	Normal,
	Displacement,
	Roughness,
	Metallic,
	AmbientOcclusion,
	BlurMap,
};