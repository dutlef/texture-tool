#pragma once

#include "./application.hpp"
#include "./shader.hpp"
#include "GLFW/glfw3.h"
#include "glad/glad.h"
#include <glm/glm.hpp>

class UIMesh
{

  public:
	unsigned int VBO, EBO, VAO;
	Shader *shader;
	Application *app;

	UIMesh(Application *app)
	{
		this->app = app;

		shader = new Shader("./shaders/ui_mesh.vert.glsl", "./shaders/ui_mesh.frag.glsl");
		shader->use();
		shader->setInt("u_texture_3dview", 0);
		shader->setInt("u_texture_2dview", 1);

		float vertices[] = {// positions      // tex coords
							1.f,  1.f,	0.f, 1.f, 1.f, 1.f,	 -1.f, 0.f, 1.f, 0.f,
							-1.f, -1.f, 0.f, 0.f, 0.f, -1.f, 1.f,  0.f, 0.f, 1.f};

		unsigned int indices[] = {0, 1, 3, 1, 2, 3};

		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		// glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindVertexArray(0);
	}

	void render()
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, app->texture_3dview);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, app->texture_2dview);

		shader->use();

		shader->setFloat("u_panel_width", app->window->panel_width);
		shader->setFloat("u_panel_height", app->window->panel_height);
		shader->setFloat("u_screen_width", app->window->width);
		shader->setFloat("u_screen_height", app->window->height);

		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
};
