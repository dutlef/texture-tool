#pragma once

#include <functional>

#include "application.hpp"
#include "imgui.h"

class UIPanel
{
	Settings *settings;
	Application *app;

  public:
	UIPanel(Application *app)
	{
		this->app = app;
	}

	void render(ImGuiIO &io, GLFWwindow *window)
	{
		auto theme = app->theme;
		auto texture = app->texture_settings;

		glfwPollEvents();

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		float margin = 20.f;

		ImGuiStyle &style = ImGui::GetStyle();
		style.FramePadding.x = 20;
		style.FramePadding.y = 12;
		style.WindowBorderSize = 0;
		style.ItemSpacing.y = 0;
		style.WindowPadding.x = 0;
		style.WindowPadding.y = 0;
		style.ChildBorderSize = 0.f;
		style.GrabMinSize = 12.f;
		style.FrameBorderSize = 1.f;
		style.FrameRounding = 1.f;
		style.GrabRounding = 1.f;

		style.Colors[ImGuiCol_WindowBg] = theme->background_darkest;
		style.Colors[ImGuiCol_Tab] = theme->background_darker;
		style.Colors[ImGuiCol_TabActive] = theme->background;
		style.Colors[ImGuiCol_TabHovered] = theme->background;
		style.Colors[ImGuiCol_SliderGrab] = hex_to_imvec4("#666666");
		style.Colors[ImGuiCol_SliderGrabActive] = hex_to_imvec4("#666666");
		style.Colors[ImGuiCol_FrameBg] = theme->background_darker;
		style.Colors[ImGuiCol_FrameBgHovered] = theme->background_darkest;
		style.Colors[ImGuiCol_FrameBgActive] = theme->background_darkest;
		style.Colors[ImGuiCol_Border] = hex_to_imvec4("#000000");
		// style.Colors[ImGuiCol_ChildBg] = ImVec4(0.0, 1.0, 0.0, 1.0);

		if (&settings->show_demo_window)
		{
			// ImGui::ShowDemoWindow(&settings->show_demo_window);
		}

		ImVec2 window_size = ImVec2(app->window->panel_width, app->window->panel_height);
		ImGui::SetNextWindowSize(window_size);
		ImGui::SetNextWindowPos(ImVec2(0, 0));
		ImGuiWindowFlags window_flags =
			ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;

		if (ImGui::Begin("Tabs", NULL, window_flags))
		{
			if (ImGui::BeginTabBar("Settings"))
			{

				tab("Clone Patching", [&texture, this]() {
					title("Patches");
					float buttonWidth = ImGui::CalcTextSize("New").x + ImGui::GetStyle().FramePadding.x * 2.0f;
					ImGui::SameLine(ImGui::GetWindowWidth() - buttonWidth - ImGui::GetStyle().WindowPadding.x - 40.f);

					if (ImGui::Button("New"))
					{
						texture->add_patch();
					}
					space_s();

					if (ImGui::BeginListBox("##listbox-patches",
											ImVec2(-FLT_MIN, 5.7 * ImGui::GetTextLineHeightWithSpacing())))
					{
						for (int n = 0; n < texture->patch_coords.size(); n++)
						{
							// auto patch = texture->patches[n];
							auto coords = texture->patch_coords[n];
							auto source = texture->patch_sources[n];
							auto size = texture->patch_sizes[n];

							const bool is_selected = (texture->current_patch_index == n);
							ImGui::SetNextItemAllowOverlap();

							auto id = fmt::format("##patch item {}", n).c_str();

							if (ImGui::Selectable(id, is_selected))
							{
								texture->current_patch_index = n;
							}

							ImGuiWindowFlags flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize |
													 ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar;

							ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.0f, 0.3, 0.0f, 0.0f));

							auto height = ImGui::GetTextLineHeightWithSpacing();

							{
								ImGui::SameLine();
								auto id = fmt::format("##name{}", n).c_str();
								ImGui::BeginChild(id, ImVec2(60, height), true, flags | ImGuiWindowFlags_NoMouseInputs);
								ImGui::Text("Patch %i", n);
								ImGui::EndChild();
							}

							ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.5f, 0.5f, 0.5f, 1.0f));

							{
								ImGui::SameLine();
								auto id = fmt::format("Position{}", n).c_str();
								ImGui::BeginChild(id, ImVec2(55, height), true, flags | ImGuiWindowFlags_NoMouseInputs);

								int x = coords.x * texture->texture_width;
								int y = coords.y * texture->texture_height;
								ImGui::Text("(%i, %i)", x, y);
								ImGui::EndChild();
							}

							{
								ImGui::SameLine();
								auto id = fmt::format("Dimensions{}", n).c_str();
								ImGui::BeginChild(id, ImVec2(196, height), true,
												  flags | ImGuiWindowFlags_NoMouseInputs);

								int x = size.x * texture->texture_width;
								int y = size.y * texture->texture_height;
								ImGui::Text("%ix%i", x, y);
								ImGui::EndChild();
							}

							{
								ImGui::SameLine();
								bool enabled = true;
								auto id = fmt::format("enabled{}", n).c_str();
								ImGui::BeginChild(id, ImVec2(15, height), true, flags);

								ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
								ImGui::PushStyleVar(ImGuiStyleVar_ItemInnerSpacing, ImVec2(0, 0));

								ImGui::Checkbox(fmt::format("##enabled{}", n).c_str(), &enabled);

								ImGui::PopStyleVar();
								ImGui::PopStyleVar();
								ImGui::EndChild();
							}

							ImGui::PopStyleColor();
							ImGui::PopStyleColor();

							ImGui::SameLine();
							ImGui::SmallButton("X");

							// Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
							if (is_selected)
							{
								ImGui::SetItemDefaultFocus();
							}
						}
						ImGui::EndListBox();
					}

					space();

					title("Active Patch");
					space_s();

					PatchSettings &patch = texture->patches[texture->current_patch_index];

					if (texture->patch_coords.size() < 1)
					{
						ImGui::Text("Create a new patch to start editing.");
						return;
					}

					int w = (float)texture->texture_width;
					int h = (float)texture->texture_height;

					glm::vec2 from(0.f, 0.f);
					glm::vec2 to(w, h);

					ImGui::Text("Coordinates");

					// texture->patch_coords[texture->current_patch_index] =
					bool changed_coords =
						slider_vec2(texture->patch_coords[texture->current_patch_index], from, to, "coords");

					space_s();

					ImGui::Text("Source");
					bool changed_source =
						slider_vec2(texture->patch_sources[texture->current_patch_index], from, to, "source");

					space_s();

					ImGui::Text("Size");
					bool changed_size =
						slider_vec2(texture->patch_sizes[texture->current_patch_index], from, to, "size");

					space_s();

					ImGui::Text("Transition");
					bool changed_sharpness = ImGui::SliderFloat("Sharpness", &patch.sharpness, 1.0f, 20.0f);

					// ImGui::Text("struct: %f", texture->patches[texture->current_patch_index].sharpness);

					space_s();

					ImGui::Text("Displacement");

					bool changed_disp_offset = ImGui::SliderFloat("Offset", &patch.displacement_offset, -1.0f, 1.0f);

					bool changed_disp_scale = ImGui::SliderFloat("Scale", &patch.displacement_scale, 0.0f, 2.0f);

					space_s();
					ImGui::Text("Base Color");

					bool changed_col_value = ImGui::SliderFloat("Value", &patch.color_value, 0.0f, 2.0f);
					bool changed_col_pow = ImGui::SliderFloat("Power", &patch.color_pow, 0.0f, 2.0f);
					bool changed_col_saturation = ImGui::SliderFloat("Saturation", &patch.color_saturation, 0.0f, 2.0f);

					if (changed_coords || changed_source || changed_size || changed_sharpness || changed_disp_offset ||
						changed_disp_scale || changed_col_value || changed_col_pow || changed_col_saturation)
					{
						app->render_preview_lowres();
					}

					space();
				});

				tab("Tiling", [texture, this]() {
					ImGui::Text("Horizontal");
					int width = texture->get_crop_width();
					bool changing =
						ImGui::SliderFloat("Start##horizontal", &texture->crop.horizontal_start, 0.0f, 1.0f);

					ImGui::SliderFloat("End##horizontal", &texture->crop.horizontal_end, 0.0f, 1.0f);
					ImGui::Text("%i px", texture->get_crop_width());

					space();

					ImGui::Text("Vertical");
					ImGui::SliderFloat("Start##vertical", &texture->crop.vertical_start, 0.0f, 1.0f);
					ImGui::SliderFloat("End##vertical", &texture->crop.vertical_end, 0.0f, 1.0f);
					// ImGui::Text("%i px",
					// &texture->get_crop_height());
					ImGui::Text("%i px", texture->get_crop_height());

					space();

					ImGui::Text("Transition");
					ImGui::SliderFloat("Sharpness", &texture->crop.transition_sharpness, 0.1f, 20.0f);
					ImGui::SliderFloat("Width", &texture->crop.transition_width, 0.0f, 1.0f);
					ImGui::SliderFloat("Offset", &texture->crop.transition_offset, 0.0f, 1.0f);
				});

				tab("Equalization", [texture, this]() {
					switch (app->blur_map_thread_status)
					{
					case BlurMapStatus::Failed:
						ImGui::Text("Couldn't create blur map.");
						break;

					case BlurMapStatus::Calculating:
						ImGui::Text("Calculating blur map.");
						break;

					case BlurMapStatus::MapCreated:
						ImGui::Text("Successfully created blur map.");
						break;

					case BlurMapStatus::MapLoaded:
						ImGui::Text("Successfully loaded blur map.");
						break;

					case BlurMapStatus::None:
						ImGui::Text("Blur map hasn't been "
									"calculated yet.");
						break;
					}

					if (app->blur_map_thread_status != BlurMapStatus::Calculating)
					{
						if (ImGui::Button("Calculate blur map"))
						{
							app->calculate_blur_map = true;
						}
					}

					if (app->blur_map_thread_status == BlurMapStatus::MapLoaded)
					{
						ImGui::SliderFloat("Value Equalization", &texture->crop.value_equalization, 0.0f, 2.0f);
					}
				});

				tab("Scene",
					[this]() { ImGui::SliderFloat("Light Rotation", &this->app->light_rotation, 0.0f, 360.0f); });

				tab("Settings", [texture, this]() {
					ImGui::SliderFloat("Tiling", &texture->tiling_scale, 0.1f, 10.0f);
					ImGui::SliderFloat("Displacement", &texture->displacement_scale, 0.0f, 5.0f);
					ImGui::SliderFloat("Normal", &texture->normal_scale, 0.1f, 10.0f);

					space();

					if (ImGui::Checkbox("Use custom export size", &app->use_custom_export_size))
					{
						app->export_size.x = app->texture_settings->get_crop_width();
						app->export_size.y = app->texture_settings->get_crop_height();
					}

					if (app->use_custom_export_size)
					{
						ImGui::SliderFloat("X##Export Size", &app->export_size.x, 2.f,
										   app->texture_settings->get_crop_width(), "%.0f");

						ImGui::SliderFloat("Y##Export SIze", &app->export_size.y, 2.f,
										   app->texture_settings->get_crop_height(), "%.0f");
					}
				});

				ImGui::EndTabBar();
			}

			ImGui::End();
		}

		// export button

		ImVec2 button_size(148, 40);
		ImGui::SetNextWindowSize(button_size);
		ImGui::SetNextWindowPos(
			ImVec2(app->window->width - button_size.x - margin, app->window->height - button_size.y - margin));

		ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 1.0, 0.0f, 0.0f));
		if (ImGui::Begin("Export", NULL, window_flags))
		{
			space();

			if (ImGui::Button("Export Texture Maps"))
			{
				app->export_textures = true;
			}
		}

		ImGui::PopStyleColor();
		ImGui::End();

		// show overlay toggle

		ImVec2 overlay_size(200, 40);
		ImGui::SetNextWindowSize(overlay_size);
		ImGui::SetNextWindowPos(ImVec2(margin, app->window->height - overlay_size.y - margin));

		ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 1.0, 0.0f, 0.0f));

		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(10.0f, 5.0f));
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10.0f, 5.0f));

		if (ImGui::Begin("Overlay", NULL, window_flags))
		{
			space();
			if (ImGui::Checkbox("Show Mask Overlay", &texture->show_color))
			{
				app->render_preview_lowres();
			}
		}

		ImGui::PopStyleVar();
		ImGui::PopStyleVar();
		ImGui::PopStyleColor();
		ImGui::End();

		// Tiling Mode

		{
			ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.0f));

			ImVec2 overlay_size(200, 40);
			ImGui::SetNextWindowSize(overlay_size);
			ImGui::SetNextWindowPos(ImVec2(app->window->panel_width - margin - overlay_size.x + 70,
										   app->window->height - overlay_size.y - margin));

			if (ImGui::Begin("Tiling Mode", NULL, window_flags))
			{
				const char *items[] = {"Both axes", "Horizontal", "Vertical"};
				static int item_current = 0;
				if (ImGui::Combo("##tiling mode", &item_current, items, IM_ARRAYSIZE(items)))
				{
					app->texture_settings->tiling_mode = static_cast<TextureSettings::TilingMode>(item_current);
				}
			}

			ImGui::PopStyleColor();

			ImGui::End();
		}

		// FPS
		{
			ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.0f));
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 1.0f, 1.0f, 0.7f));

			ImVec2 overlay_size(ImGui::CalcTextSize("FPS: 00").x, ImGui::CalcTextSize("FPS: 00").y);
			ImGui::SetNextWindowSize(overlay_size);
			ImGui::SetNextWindowPos(ImVec2(app->window->width - margin - overlay_size.x, margin));

			ImGui::Begin("FPS", NULL, window_flags);

			ImGui::Text("FPS: %i", (int)app->get_fps());

			ImGui::PopStyleColor();
			ImGui::PopStyleColor();

			ImGui::End();
		}

		ImGui::Render();
	}

  private:
	void space()
	{
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
	}

	void space_s()
	{
		ImGui::Spacing();
	}

	bool slider_vec2(glm::vec2 &normalized_source, glm::vec2 from, glm::vec2 to, std::string id)
	{
		int x = normalized_source.x * (to.x - from.x);
		int y = normalized_source.y * (to.y - from.y);
		auto idx = fmt::format("X##{}", id);
		auto idy = fmt::format("Y##{}", id);
		// std::printf("id: %s   %s\n", idx, idy);

		bool changed_x = ImGui::SliderInt(idx.c_str(), &x, from.x, to.x);
		bool changed_y = ImGui::SliderInt(idy.c_str(), &y, from.y, to.y);

		normalized_source.x = x / (to.x - from.x);
		normalized_source.y = y / (to.y - from.y);

		return changed_x || changed_y;
	}

	void title(std::string text)
	{
		ImGui::PushFont(app->bold_font);
		ImGui::Text(text.c_str());
		ImGui::PopFont();
	}

	void tab(const char *name, std::function<void()> content)
	{
		if (ImGui::BeginTabItem(name))
		{
			ImGui::PushStyleColor(ImGuiCol_ChildBg, app->theme->background);

			auto flags = ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;

			if (ImGui::BeginChild("Child1", ImVec2(0, 0), true, flags))
			{

				if (ImGui::BeginChild("Child1", ImVec2(ImGui::GetWindowSize().x - 0.f, 0), true, flags))
				{

					ImGui::Indent(20.0f);

					ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(10.0f, 5.0f));
					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10.0f, 5.0f));

					ImGui::BeginTable("Table", 2, ImGuiTableFlags_SizingFixedFit | ImGuiTableFlags_NoBordersInBody);

					ImGui::TableSetupColumn("Content", ImGuiTableColumnFlags_WidthStretch);
					ImGui::TableSetupColumn("Space for scrollbar", ImGuiTableColumnFlags_WidthFixed, 20.0f);

					ImGui::TableNextRow();

					ImGui::TableSetColumnIndex(0);
					space();
					content();

					ImGui::EndTable();

					ImGui::PopStyleVar();
					ImGui::PopStyleVar();
					ImGui::EndChild();
				}

				ImGui::EndChild();
			}

			ImGui::PopStyleColor();

			ImGui::EndTabItem();
		}
	}
};