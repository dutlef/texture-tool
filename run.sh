#!/usr/bin/env bash

cmake --preset=default || exit 1
cmake --build build || exit 1
./build/TextureTool || exit 1