#version 330 core

uniform vec3 u_light_color;
uniform vec3 u_object_color;
uniform vec3 u_light_pos;  

uniform sampler2D u_cropped_color;
uniform sampler2D u_cropped_displacement;
uniform sampler2D u_cropped_normal;
uniform sampler2D u_cropped_rough;
uniform sampler2D u_cropped_metal;
uniform sampler2D u_cropped_ao;

uniform float u_tiling_scale;
uniform float u_normal_scale;
uniform float u_light_rotation;

uniform int u_view_mode;
uniform bool u_shaded;

uniform float u_crop_width;
uniform float u_crop_height;

in vec2 v_tex_coords;
in vec3 v_normal;	
in vec3 v_frag_pos;  
in mat3 TBN;
in vec3 v_tangent;
in vec3 v_bitangent;

out vec4 color;


vec3 rotate_light(vec3 dir)
{
    float theta = radians(u_light_rotation - 180.0);
    dir.x = dir.x * cos(theta) + dir.z * sin(theta);
    dir.z = -dir.x * sin(theta) + dir.z * cos(theta);

    return dir;
}

void main()
{
    vec3 col = vec3(1.0, 0.0, 1.0);

    float ratio = u_crop_width / u_crop_height;
    
    // float ratio = 1.0;

    vec2 uvs = v_tex_coords * vec2(u_tiling_scale) * vec2(1.0, ratio);

    if (u_shaded)
    {
        vec3 normal = texture(u_cropped_normal, uvs).rgb;
        normal = mix(vec3(0.5, 0.5, 1.0), normal, u_normal_scale);
		normal.y = 1.0 - normal.y;
		normal = normal * 2.0 - 1.0;   
        // normal.x *= -1.0;
        normal = normalize(TBN * normal);


        vec3 diff = vec3(0.05);
        vec3 lightDir = normalize(vec3(12.0, 12.0, 12.0) - v_frag_pos);

        lightDir = rotate_light(lightDir);

        diff += max(dot(lightDir, normal), 0.0) * 2.0;

        vec3 lightDir2 = normalize(vec3(-12.0, 4.0, 6.0) - v_frag_pos);
        lightDir2 = rotate_light(lightDir2);


        diff += max(dot(lightDir2, normal), 0.0) * 0.1;

        vec3 base_color = texture(u_cropped_color, uvs).rgb;
        base_color += vec3(0.1);

        vec3 diffuse = diff * base_color;

        diffuse = tanh(diffuse);

        col = diffuse;
    }
    else if (u_view_mode == 0)
    {
        col = texture(u_cropped_color, uvs).rgb;
    }
    else if (u_view_mode == 1)
    {
        col = texture(u_cropped_displacement, uvs).rgb;
    }
    else if (u_view_mode == 2)
    {
        col = texture(u_cropped_normal, uvs).rgb;
	}
	else if (u_view_mode == 3)
	{
		col = texture(u_cropped_rough, uvs).rgb;
	}
	else if (u_view_mode == 4)
	{
		col = texture(u_cropped_metal, uvs).rgb;
	}
	else if (u_view_mode == 5)
	{
		col = texture(u_cropped_ao, uvs).rgb;
	}
	else
	{
		col = vec3(1.0, 0.0, 1.0);
	}

	color = vec4(col, 1.0);
	// color = vec4(vec3(1.0, 0.0, 1.0), 1.0);
}
