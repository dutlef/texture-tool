#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec3 a_tangent;
layout (location = 4) in vec3 a_bitangent;

out vec2 v_tex_coords;
out vec3 v_normal;
out vec3 v_frag_pos;  
out mat3 TBN;
out vec3 v_tangent;
out vec3 v_bitangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float u_crop_width;
uniform float u_crop_height;

uniform float u_displacement_scale;
uniform float u_tiling_scale;

uniform sampler2D u_cropped_displacement;


void main()
{
	v_tex_coords = aTexCoord;
	// v_normal = aNormal;

	float ratio = u_crop_width / u_crop_height;
	vec2 uvs = v_tex_coords * vec2(u_tiling_scale) * vec2(1.0, ratio);

	float displacement = texture(u_cropped_displacement, uvs).r;

	vec3 pos = aPos + ((displacement - 0.5) * u_displacement_scale * aNormal);

	v_tangent = a_tangent;
	v_bitangent = a_bitangent;

	vec3 T = normalize(vec3(model * vec4(a_tangent,   0.0)));
   	vec3 B = normalize(vec3(model * vec4(a_bitangent, 0.0)));
   	vec3 N = normalize(vec3(model * vec4(aNormal,    0.0)));
   	TBN = mat3(T, B, N);


	v_normal = mat3(transpose(inverse(model))) * aNormal; // https://learnopengl.com/Lighting/Basic-Lighting
	v_frag_pos = vec3(model * vec4(pos, 1.0));
	gl_Position = projection * view * model * vec4(pos, 1.0);
}
