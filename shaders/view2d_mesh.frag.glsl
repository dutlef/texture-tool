#version 330 core

out vec4 FragColor;

in vec2 v_tex_coords;
in vec4 v_gl_pos;

uniform float u_view2d_height;
uniform float u_view2d_width;

uniform float u_vertical_start;
uniform float u_vertical_end;
uniform float u_horizontal_start;
uniform float u_horizontal_end;

uniform bool u_colorize;

uniform float u_transition_width;
uniform float u_transition_sharpness;
uniform float u_transition_offset;

uniform vec3 u_background_darker;

uniform sampler2D u_texture_col;
uniform sampler2D u_texture_disp;
uniform sampler2D u_texture_nrm;
uniform sampler2D u_texture_metal;
uniform sampler2D u_texture_rough;
uniform sampler2D u_texture_ao;
uniform sampler2D u_texture_blur;

uniform bool u_crop;
uniform int u_map_index;

uniform int u_tiling_mode;
#define TILING_MODE_BOTH_AXES 0
#define TILING_MODE_HORIZONTAL 1
#define TILING_MODE_VERTICAL 2

uniform float u_value_equalization;

// uniform float u_screen_width;
// uniform float u_screen_height;

uniform int u_patch_count;
uniform int u_active_patch;
uniform vec2 u_patch_coords[10];
uniform vec2 u_patch_sources[10];
uniform vec2 u_patch_sizes[10];
uniform vec3 u_patch_other1[10];
uniform vec3 u_patch_other2[10];
uniform float u_patch_sharpnesses[10];
uniform float u_patch_disp_offset[10];
uniform float u_patch_disp_scale[10];
uniform float u_patch_color_value[10];
uniform float u_patch_color_saturation[10];
uniform float u_patch_color_pow[10];

vec2 plane_coords;
vec2 plane_coords_right;
vec2 plane_coords_top;
vec2 plane_coords_corner;

float right_mask;
float top_mask;
float corner_mask;
float patch_masks[10];

float scale = 0.9;

float masks[10];

vec2 get_responsive_plane_coords(float scale)
{

    vec2 square_coords = v_tex_coords;
    square_coords -= vec2(0.5);
    square_coords.y *= (u_view2d_height / u_view2d_width);

    if (u_view2d_height < u_view2d_width) {
        square_coords /= vec2(u_view2d_height / u_view2d_width);
    }

    square_coords /= scale;

    square_coords += vec2(0.5);

    return square_coords;
}

vec3 draw_line(vec3 color, float value, bool vertical)
{
    float plane_width_in_pixels = min(u_view2d_width, u_view2d_height) * scale;
    float pixel_in_plane = 1.0 / plane_width_in_pixels;
    float line_width_in_pixels = 2.0;

    float plane_coord = plane_coords.x;
    float view2d_size = u_view2d_height;
    float tex_coord = v_tex_coords.y;

    if (vertical) {
        plane_coord = plane_coords.y;
        view2d_size = u_view2d_width;
        tex_coord = v_tex_coords.x;
    }

    if (abs(plane_coord - value) < line_width_in_pixels * pixel_in_plane * 0.5)
    {
        float v = tex_coord * view2d_size / 4.0;
        v = mod(v, 1.0);
        vec3 line = vec3(0.5, 0.0, 1.0);
        if (v > 0.5) 
        {
            line = vec3(0.5, 1.0, 0.0);
        }
        color = mix(color, line, 0.5);
    }

    return color;
}

float grayscale(vec3 rgb)
{
	return 0.299 * rgb.r + 0.587 * rgb.g + 0.114 * rgb.b;
}

vec3 multisample(sampler2D texture_map, vec3 add_to_base)
{
    vec2 multiplier = vec2(1.0);

	vec3 combined = texture(texture_map, plane_coords * multiplier).rgb + add_to_base;

	if (u_tiling_mode == TILING_MODE_BOTH_AXES || u_tiling_mode == TILING_MODE_HORIZONTAL)
	{
		vec3 right = texture(texture_map, plane_coords_right * multiplier).rgb;
		combined = mix(right, combined, right_mask);
	}

	if (u_tiling_mode == TILING_MODE_BOTH_AXES || u_tiling_mode == TILING_MODE_VERTICAL)
	{
		vec3 top = texture(texture_map, plane_coords_top * multiplier).rgb;
        combined = mix(top, combined, top_mask);
	}

	if (u_tiling_mode == TILING_MODE_BOTH_AXES)
    {
        vec3 corner = texture(texture_map, plane_coords_corner * multiplier).rgb;
        combined = mix(corner, combined, corner_mask);
    }

	for (int i = 0; i < 10; i++)
	{

		if (i >= u_patch_count)
		{
			continue;
		}

		vec2 coords = (u_patch_sources[i] - u_patch_coords[i]) + plane_coords;
		vec3 _patch = texture(texture_map, coords).rgb;

		if (u_map_index == 0)
		{
			_patch *= u_patch_color_value[i];
			_patch = pow(_patch, vec3(u_patch_color_pow[0]));
			_patch = mix(vec3(grayscale(_patch)), _patch, u_patch_color_saturation[i]);
			_patch = _patch + add_to_base;
		}

		combined = mix(_patch, combined, patch_masks[i]);

		// FragColor = vec4(displacement_combined, 1.0);
		// // FragColor = vec4(vec3(mask), 1.0);
		// FragColor = vec4(vec3(i / 10), 1.0);

		// masks[i] = dist;
	}

	return combined;
}


vec3 multisample(sampler2D texture_map)
{
    return multisample(texture_map, vec3(0));
}

float clamp01(float value)
{
    return clamp(value, 0.0, 1.0);
}

vec3 clamp01(vec3 vec)
{
	return vec3(clamp01(vec.x), clamp01(vec.y), clamp01(vec.z));
}

void main()
{
    float transition_width = max(0.001, u_transition_width);
    plane_coords = get_responsive_plane_coords(scale);

   

    if (u_crop)
    {
        // adjust coords so that only the rectangle within the lines will be rendered
        plane_coords = v_tex_coords;
        plane_coords.x *= u_horizontal_end - u_horizontal_start;
        plane_coords.x +=  u_horizontal_start;
        plane_coords.y *= u_vertical_end - u_vertical_start;
        plane_coords.y += u_vertical_start;
    }



    vec3 color = u_background_darker * 0.5;

	vec2 crop_coords = vec2(
		(plane_coords.x - u_horizontal_start) / (u_horizontal_end - u_horizontal_start),
		(plane_coords.y - u_vertical_start) / (u_vertical_end - u_vertical_start)
	);


    if (abs(plane_coords.x - 0.5) < 0.5 && abs(plane_coords.y - 0.5) < 0.5)
    {
        color = vec3(plane_coords, 0.0);
        vec3 displacement = texture(u_texture_disp, plane_coords).rrr;
        vec3 normal = texture(u_texture_nrm, plane_coords).rgb;

        plane_coords_right = plane_coords - vec2(u_horizontal_end - u_horizontal_start, 0.0);
        plane_coords_top = plane_coords - vec2(0.0, u_vertical_end - u_vertical_start);
        plane_coords_corner = plane_coords - vec2(u_horizontal_end - u_horizontal_start, u_vertical_end - u_vertical_start);

        vec3 displacement_right = texture(u_texture_disp, plane_coords_right).rrr;
        vec3 displacement_top = texture(u_texture_disp, plane_coords_top).rrr;
        vec3 displacement_corner = texture(u_texture_disp, plane_coords_corner).rrr;

		float nudged_transition_offset = u_transition_offset;

		nudged_transition_offset *= clamp01(crop_coords.x / 0.2);
		nudged_transition_offset *= clamp01(crop_coords.y / 0.2);

        float right_gradient = plane_coords.x - u_horizontal_end - nudged_transition_offset;
        right_gradient *= -1.0;
        right_gradient /= transition_width;

        float top_gradient = plane_coords.y - u_vertical_end - nudged_transition_offset;
        top_gradient *= -1.0;
        top_gradient /= transition_width;

        float corner_gradient = distance((plane_coords), vec2(u_horizontal_end, u_vertical_end)) * 1.0;
        corner_gradient /= transition_width;

		FragColor = vec4(corner_gradient);
		// return;

		// top_gradient -= right_gradient;

		FragColor = vec4(top_gradient);
		FragColor = vec4(top_gradient + clamp01(1.0 - right_gradient));
		// FragColor = vec4(min(top_gradient, (1.0 - right_gradient)));
		// top_gradient = min(top_gradient, right_gradient);

		if (u_tiling_mode != TILING_MODE_VERTICAL)
		{
			top_gradient += clamp01(1.0 - right_gradient);
		}
		// return;

		// right_gradient = min(right_gradient, 1.0);
		// right_gradient = clamp01(right_gradient);

		// color = (displacement * right_gradient);
		// color = (displacement_right * (1.0 - right_gradient));

		vec3 displacement_combined = displacement;

		FragColor = vec4(displacement_combined, 1.0);
		// return;

		float right_diff = displacement_combined.x * right_gradient - displacement_right.x * (1.0 - right_gradient);
		right_mask = (right_diff - 0.5) * u_transition_sharpness + 0.5;
		right_mask = clamp01(right_mask);

		displacement_combined = mix(displacement_right, displacement_combined, right_mask);

		float top_diff = displacement_combined.x * top_gradient - displacement_top.y * (1.0 - top_gradient);
		top_mask = (top_diff - 0.5) * u_transition_sharpness + 0.5;

		top_mask = clamp01(top_mask);

		displacement_combined = mix(displacement_top, displacement_combined, top_mask);

		float corner_diff = displacement_combined.x * corner_gradient - displacement_corner.z * (1.0 - corner_gradient);
		corner_mask = (corner_diff - 0.5) * u_transition_sharpness + 0.5;
		corner_mask = clamp01(corner_mask); // TODO: fix black spots

		displacement_combined = mix(displacement_corner, displacement_combined, corner_mask);

		vec3 overlay = vec3(1.0);

		if (u_tiling_mode == TILING_MODE_BOTH_AXES || u_tiling_mode == TILING_MODE_HORIZONTAL)
		{
			overlay = mix(vec3(1.0, 0.0, 0.0), overlay, right_mask);
		}

		if (u_tiling_mode == TILING_MODE_BOTH_AXES || u_tiling_mode == TILING_MODE_VERTICAL)
		{
			overlay = mix(vec3(0.0, 1.0, 0.0), overlay, top_mask);
		}

		if (u_tiling_mode == TILING_MODE_BOTH_AXES)
		{
			overlay = mix(vec3(0.0, 0.0, 1.0), overlay, corner_mask);
		}

		// TODO: research about loops, constant array indexes and so on
		for (int i = 0; i < 10; i++)
		{
			if (u_patch_count < i - 1)
			{
				FragColor = vec4(vec3(i / 20.0, 0.0, 0.0), 1.0);

				continue;
			}

			vec2 patch_coords = (plane_coords - u_patch_coords[i]) / u_patch_sizes[i];
			vec2 source_coords = (plane_coords - u_patch_sources[i]);

			float dist = 1.0 - distance(vec2(0.0), patch_coords) + 1.0;

			vec2 coords = (u_patch_sources[i] - u_patch_coords[i]) + plane_coords;
			vec3 displacement_patch = texture(u_texture_disp, coords).rrr;

			displacement_patch -= 0.5;
			displacement_patch *= u_patch_disp_scale[i];
			displacement_patch += 0.5;

			displacement_patch += u_patch_disp_offset[i];

			displacement_patch = clamp01(displacement_patch) * 0.99 + 0.005;

			float gradient = distance(vec2(0.0), patch_coords) - 0.5;

			float diff = displacement_combined.x * gradient - displacement_patch.r * (1.0 - gradient);
			float patch_transition_sharpness = 1.0;
			patch_transition_sharpness = max(1.0, u_patch_sharpnesses[i]);
			// patch_transition_sharpness = u_patch_sharpnesses[0];
			float mask = (diff - 0.5) * patch_transition_sharpness + 0.5;
			mask = clamp01(mask);
			// mask += u_patch_sharpnesses[i];

			FragColor = vec4(vec3(diff), 1.0);
			FragColor = vec4(vec3(mask), 1.0);
			displacement_combined = mix(displacement_patch, displacement_combined, mask);

			patch_masks[i] = mask;
			// FragColor = vec4(displacement_combined, 1.0);
			// // FragColor = vec4(vec3(mask), 1.0);
			// FragColor = vec4(vec3(i / 10), 1.0);

			// masks[i] = dist;
			FragColor = vec4(vec3(i / 20.0, 1.0, 0.0), 1.0);
		}

		if (u_map_index == 0)
		{
            // vec3 diffuse = texture(u_texture_col, plane_coords).rgb;
            // vec3 diffuse_right = texture(u_texture_col, plane_coords_right).rgb;
            // vec3 diffuse_top = texture(u_texture_col, plane_coords_top).rgb;
            // color = mix(diffuse_right, diffuse, right_mask);
            // color = mix(diffuse_top, color, top_mask);

                // diff zwüsche grüenem, top render und base, aber blur

            vec2 multiplier = vec2(1.0);


            // vec3 base = texture(u_texture_col, plane_coords * multiplier).rgb;
            // vec3 right = texture(u_texture_col, plane_coords_right * multiplier).rgb;
            // vec3 top = texture(u_texture_col, plane_coords_top * multiplier).rgb;
            // vec3 corner = texture(u_texture_col, plane_coords_corner * multiplier).rgb;

            vec2 blur_coords = plane_coords;
            blur_coords.y = 1.0 - 0.5 * u_vertical_start;
            vec3 base_blur = texture(u_texture_blur, blur_coords * multiplier).rgb;

            vec2 blur_coords_top = plane_coords_top;
            blur_coords_top.y = 0.5 * u_vertical_start;
            vec3 top_blur = texture(u_texture_blur, blur_coords_top * multiplier).rgb;

            vec3 diff_base_top =  top_blur - base_blur;

			vec2 equalize_gradient_uvs =
				(plane_coords - vec2(0.0, u_vertical_start)) / vec2(1.0, u_vertical_end - u_vertical_start);

			vec3 to_add = vec3(grayscale(diff_base_top) * u_value_equalization * clamp01(equalize_gradient_uvs.y));

			// vec3 combined = mix(right, base, right_mask);
			// combined = mix(top, combined, top_mask);
			// combined = mix(corner, combined, corner_mask);

			// vec3 diff =

			color = multisample(u_texture_col, to_add);

			// color = to_add + vec3(1.0);

			// color = vec3(equalize_gradient_uvs, 0.0);

			// color = diff_base_top * vec3(12.0);
			// color = top_blur - base_blur;
			// color = top_blur + 0.5;
			// color = multisample(u_texture_col, false, false);

			// vec3 blur = multisample(u_texture_blur, false, true);

			// float grayscale = 0.299 * blur.r + 0.587 * blur.g + 0.114 * blur.b;

			// grayscale -= 0.5;
			// grayscale *= u_value_equalization;

			// color *= vec3(1.0 + grayscale);
		}
		else if (u_map_index == 1)
		{
			// vec3 combined = mix(displacement_right, displacement, right_mask);
			// combined = mix(displacement_top, combined, top_mask);
			color = displacement_combined;
		}
		else if (u_map_index == 2)
		{
			color = multisample(u_texture_nrm);
			vec3 unmapped = color * 2.0 - 1.0;
			color = normalize(unmapped) / 2.0 + 0.5; 
		}
		else if (u_map_index == 3)
		{
			color = multisample(u_texture_rough).rrr;
		}
		else if (u_map_index == 4)
		{
			color = multisample(u_texture_metal).rrr;
		}
		else if (u_map_index == 5)
		{
			color = multisample(u_texture_ao).rrr;
		}
		else 
        {
            color = vec3(1.0, 0.0, 1.0);
        }


          
       	if (u_colorize)
		{
			color = mix(color, color * overlay, 0.5);
		}
          
            // color = vec3(1.0 - right_gradient);
            


            // if (plane_coords.x > u_horizontal_end)
            // {
            //     color = vec3(plane_coords_right, 0.0);
            //     color = texture(u_texture_col, plane_coords_right).rgb;
            // }

            // if (plane_coords.y > u_vertical_end)
            // {
            //     color = vec3(plane_coords_top, 0.0);
            //     color = texture(u_texture_col, plane_coords_top).rgb;
            // }

    }


    if (!u_crop)
    {
        // start and end lines
        color = draw_line(color, u_horizontal_start, false);
        color = draw_line(color, u_horizontal_end, false);
        color = draw_line(color, u_vertical_start, true);
		color = draw_line(color, u_vertical_end, true);

		// patches overlay
		vec2 patch_coords = (plane_coords - u_patch_coords[u_active_patch]) / u_patch_sizes[u_active_patch];
		vec2 source_coords = (plane_coords - u_patch_sources[u_active_patch]);

		float dist = distance(vec2(0.0), patch_coords) * 0.5;
		float circle = 1.0 - abs(dist - 1.0) * 16.0;
		circle = clamp01(circle);

		float source_cross = 0.0;

		bool on_line = abs(source_coords.x) < 0.005 || abs(source_coords.y) < 0.005;
		bool in_bounds = distance(source_coords, vec2(0.0)) < 0.05;

		if (on_line && in_bounds)
		{
			source_cross = 1.0;
		}

		// color = vec3(masks[u_active_patch]);
		// color = mix(color, vec3(masks[u_active_patch]), clamp01(1.0 - dist + 1.0));

		// color = vec3(dist);
		// color = vec3(circle);
		// color = vec3(patch_coords, 0.0);

		color = mix(color, vec3(1.0, 1.0, 0.5), circle);
		color = mix(color, vec3(0.5, 1.0, 1.0), source_cross);
	}

	FragColor = vec4(color, 1.0);
}