#version 330 core
layout (location = 0) in vec3 a_pos;
layout (location = 1) in vec2 a_tex_coords;

out vec2 v_tex_coords;
out vec4 v_gl_pos;

uniform bool u_flip_y;

void main()
{
    gl_Position = vec4(a_pos, 1.0);

    v_tex_coords = a_tex_coords;
    if (u_flip_y)
    {
        v_tex_coords.y = 1.0 - v_tex_coords.y;
    }

    v_gl_pos = gl_Position;
}