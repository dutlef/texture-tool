#version 330 core
out vec4 FragColor;

in vec2 v_tex_coords;
in vec4 v_gl_pos;

uniform float u_panel_width;
uniform float u_panel_height;

uniform float u_screen_width;
uniform float u_screen_height;

uniform sampler2D u_texture_3dview;
uniform sampler2D u_texture_2dview;


void main()
{
    vec2 pixel_coords = v_tex_coords * vec2(u_screen_width, u_screen_height);
    // vec3 color = vec3(0.4, 0.3, 0.2);

    vec2 view3d_coords = v_tex_coords;
    view3d_coords.x -= u_panel_width / u_screen_width;
    view3d_coords.x /= (u_screen_width - u_panel_width) / u_screen_width;

    vec3 color = texture(u_texture_3dview, view3d_coords).rgb;

    // color = vec3(view3d_coords, 0.0);

    vec2 view2d_coords = v_tex_coords;
    // view2d_coords.x += (u_screen_width - u_panel_width) / u_screen_width;
    view2d_coords.x /= u_panel_width / u_screen_width;
    view2d_coords.y /= (u_screen_height - u_panel_height) / u_screen_height;

    float left_panel = 0.0;

    vec3 view2d = texture(u_texture_2dview, view2d_coords).rgb;
    // view2d = vec3(1.0, 1.0, 0.0);
    // color = vec3(view2d_coords, 0.0);

    vec3 left_side = vec3(0.0);

    if (pixel_coords.y < u_screen_height - u_panel_height - 2.0)
    {
        left_side = view2d;
    }

    if (pixel_coords.x < u_panel_width + 2.0)
    {
        color = vec3(0.0);
    }

    if (pixel_coords.x < u_panel_width)
    {
        color = left_side;
    }

    
    // FragColor = vec4(v_tex_coords, 0.0, 1.0);
    FragColor = vec4(color, 1.0);
}